# Java 8


## Interface methods

* Interface static method
* Interface default method


## Method references

Static method reference

    ClassName::staticMethod

Instance method reference

    objectInstance::instanceMethod

Object type method reference

    ObjectType::method

Constructor reference

    ClassName::new




## Lambda expression

Functional interface = one method interface

    (params) -> { /* method body */ }



## java.time



## `Optional<T>`

    Optional<String> optional = Optional.empty();
    Optional<String> optional = Optional.of(str);
    Optional<String> optional = Optional.ofNullable(str);

    o.orElse()
    o.orElseGet()
    o.orElseThrow()
    ...



## `CompleteableFuture<T>`




## Java stream API


    Stream<String> s1 = Arrays.stream(arr);
    Stream<String> s2 = Stream.of("a", "b", "c");
    Stream<String> s3 = list.stream();
    Stream<String> s4 = Stream.<String>builder().add("a").add("b").add("c").build();
    Stream<String> s5 = Stream.generate(() -> "a").limit(10);
    Stream<String> s6 = Stream.iterate(10, n -> n + 1).limit(10);
    Stream<String> s7 = list.parallelStream();
    Stream<String> s8 = IntStream.range(1, 150).parallel();
    parallelStream.sequential();

    s.filter()
    s.map()
    s.flatMap()
    s.anyMatch()
    s.allMatch()
    s.noneMatch()
    s.reduce(), count(), min(), max(), ...
    s.collect(Collectors.toList())



## Nashorn - JVM Javascript engine


...



